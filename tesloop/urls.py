from django.conf.urls import include, url
from django.contrib import admin
from rest_framework import routers

from drivers import views

router = routers.DefaultRouter()
router.register(r'teslooptest/users',
        views.UserViewSet,
        base_name='users')
router.register(r'teslooptest/users/(?P<user_id>\d+)/trips',
        views.TripViewSet,
        base_name='trips')

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
