from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
    user = models.OneToOneField(User)
    is_driver = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username

class Trip(models.Model):
    driver = models.ForeignKey(User)
    name = models.CharField(max_length=255)
    departure_time = models.DateTimeField()
