import django_filters

from drivers.models import Trip


class TripFilter(django_filters.rest_framework.FilterSet):
    start = django_filters.DateFilter(name='departure_time', lookup_expr='gte')
    end = django_filters.DateFilter(name='departure_time', lookup_expr='lte')

    class Meta:
        model = Trip
        fields = ('start', 'end')
