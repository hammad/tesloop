from rest_framework import serializers
from django.contrib.auth.models import User

from drivers.models import Trip, Profile


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('is_driver',)


class UserSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer()

    class Meta:
        model = User
        fields = ('id', 'username', 'profile')


class TripSerializer(serializers.ModelSerializer):
    class Meta:
        model = Trip
        fields = ('id', 'name', 'departure_time')
        read_only_fields = ('id', 'driver')

    def create(self, validated_data):
        validated_data['driver_id'] = self.context['view'].kwargs['user_id']
        return super(TripSerializer, self).create(validated_data)
