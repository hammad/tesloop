from datetime import datetime, timedelta
from rest_framework.test import APITestCase
from rest_framework import status

from django.contrib.auth.models import User
from django.urls import reverse

from drivers.factories import TripFactory

class DriversTestCase(APITestCase):

    def setUp(self):
        self.superuser = User.objects.create_superuser(
                        'test', 'test@test.com', 'test123')
        self.client.login(username='test', password='test123')

        self.superuser.profile.is_driver = True
        self.superuser.profile.save()


    def test_endpoint_locked_to_non_drivers(self):
        """
        Tests that non drivers cannot use the /trips/ endpoint
        """
        self.superuser.profile.is_driver = False
        self.superuser.profile.save()

        route = reverse('trips-list', kwargs={'user_id': self.superuser.id})
        response = self.client.get(route)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_endpoint_available_to_drivers(self):
        """
        Tests that drivers can use the /trips/ endpoint
        """
        TripFactory(driver=self.superuser)
        TripFactory(driver=self.superuser)

        route = reverse('trips-list', kwargs={'user_id': self.superuser.id})
        response = self.client.get(route)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            len(response.data),
            2
        )

    def test_endpoint_only_available_to_own_user(self):
        """
        Tests that only the authenticated user can see his/her own trips.
        """
        another_user = User.objects.create_superuser(
            'another', 'another@test.com', 'another')

        another_user.profile.is_driver = True
        another_user.profile.save()

        route = reverse('trips-list', kwargs={'user_id': another_user.id})
        response = self.client.get(route)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_filter_by_departure_time(self):
        """
        Tests that drivers can filter their trips by departure times
        """
        now = datetime.now()

        trip = TripFactory(driver=self.superuser, departure_time=now)
        TripFactory(
            driver=self.superuser,
            departure_time=now - timedelta(days=2)
        )
        TripFactory(
            driver=self.superuser,
            departure_time=now+ timedelta(days=2)
        )

        route = reverse('trips-list', kwargs={'user_id': self.superuser.id})
        response = self.client.get(route)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            len(response.data),
            3
        )

        start = now - timedelta(days=1)
        end = now + timedelta(days=1)
        params = {
            'start': '{}-{}-{}'.format(start.year, start.month, start.day),
            'end': '{}-{}-{}'.format(end.year, end.month, end.day),
        }
        response = self.client.get(route, params)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            len(response.data),
            1
        )
        self.assertEqual(response.data[0]['id'], trip.id)
