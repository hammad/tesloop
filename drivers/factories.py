import datetime
import factory
#from django.db.models import signals

#from django.contrib.auth.models import User
from drivers.models import Profile, Trip

class TripFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Trip

    #fdt = factory.fuzzy.FuzzyDateTime(datetime.datetime(2008, 1, 1))
    name = factory.Faker('word')
    departure_time = factory.Faker('date_time')
