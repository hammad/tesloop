import django_filters

from rest_framework import viewsets, permissions, mixins, exceptions
from django.contrib.auth.models import User

from drivers.models import Trip
from drivers.serializers import TripSerializer, UserSerializer
from drivers.filters import TripFilter
from drivers.permissions import IsDriverPermission


class UserViewSet(
        mixins.RetrieveModelMixin,
        mixins.ListModelMixin,
        viewsets.GenericViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated,)


class TripViewSet(viewsets.ModelViewSet):
    serializer_class = TripSerializer
    permission_classes = (permissions.IsAuthenticated, IsDriverPermission)
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
    filter_class = TripFilter

    def get_queryset(self):
        requested_user_id = int(self.kwargs['user_id'])
        if requested_user_id != self.request.user.id:
            raise exceptions.PermissionDenied

        return Trip.objects.filter(driver=requested_user_id)
