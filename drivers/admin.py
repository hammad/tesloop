from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from drivers.models import Profile, Trip


class TripAdmin(admin.ModelAdmin):
    model = Trip


class ProfileAdmin(admin.ModelAdmin):
    model = Profile
    list_display = ('id', 'user', 'is_driver')


admin.site.register(Profile, ProfileAdmin)
admin.site.register(Trip, TripAdmin)
