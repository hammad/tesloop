teslooptest
===========

A small RESTful API that allows drivers to query their scheduled trips and
filter by their departing times.

# Stack

sqllite, Python3, Django 1.11

# Usage

Run migrations on your db, create a user, and spin up a runserver.

```
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver 8001
```
Note: You may have to use `python3` instead of `python` depending on how you
have python 2 and 3 set up on your system. Same goes for any virtualenvs or pip
installations.

You can then interact with the API at http://localhost:8001/. Admin is
available at http://localhost:8001/admin.

Run tests by doing `python manage.py test`

# Endpoints

`GET /teslooptest/users/` - Lists all users.

`GET /teslooptest/users/<user_id>/trips/` - See list of trips associated with a
driver. Requires authed user to be a driver, and authed user to be requesting
their own trips.

`POST /teslooptest/users/<user_id>/trips/` - Create a trip for the driver. Same
restrictions apply.
